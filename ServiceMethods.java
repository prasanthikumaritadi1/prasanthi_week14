package com.week14.LibraryService;

import com.week14.LibraryEntity.Books;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public interface ServiceMethods 
{

	List<Books> getBooksByAscendingOrder();
    List<Books> getAllBooks();
    
    List<Books> getByAuthor(String author);
    
    List<Books> getByName(String bookName);
    public Books getByLastAdded();
    
    Books getById(int bookId);
    
    
    List<Books> addLibraryBooks(List<Books> books);
    void deleteById(int bookid);

    void deleteAllBooks();



}
