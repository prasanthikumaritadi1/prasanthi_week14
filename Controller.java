package com.week14.LibraryController;

import com.week14.LibraryEntity.Books;
import com.week14.LibraryService.ServiceMethods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequestMapping("/api")
public class Controller {
	
	@Autowired
	private ServiceMethods servicemethods;

	@GetMapping("/getBooks")
	public List<Books> getAllBooks() {
		return servicemethods.getAllBooks();
	}

	@GetMapping("/getBook/{bookID}")
	public Books getBookById(@PathVariable int bookID) {
		return servicemethods.getById(bookID);
	}

	@GetMapping("/getBooksByName/{bookName}")
	public List<Books> getByName(@PathVariable String bookName) {
		return servicemethods.getByName(bookName);
	}

	@GetMapping("/getBooksByLastAdded")
	public Books getByLastAdded() {
		return servicemethods.getByLastAdded();
	}

	@GetMapping("/getByAuthor/{author}")
	public List<Books> getBookByAuthor(@PathVariable String author) {
		return servicemethods.getByAuthor(author);
	}

	@GetMapping("/getBooksByAscOrd")
	public List<Books> getBooksByAscendingOrder() {
		return servicemethods.getBooksByAscendingOrder();
	}

	@PostMapping("/addBooks")
	public List<Books> addBooks(@RequestBody List<Books> books) {
		return servicemethods.addLibraryBooks(books);
	}

	@DeleteMapping("/deleteAllBooks")
	public ResponseEntity<String> deleteAllBooks() {
		servicemethods.deleteAllBooks();
		return new ResponseEntity<String>("records are deleted", HttpStatus.OK);
	}

	@DeleteMapping("/deleteById/{bookId}")
	public ResponseEntity<String> deleteById(@PathVariable int bookId) {
		servicemethods.deleteById(bookId);
		return new ResponseEntity<String>("record is deleted", HttpStatus.OK);
	}


}
