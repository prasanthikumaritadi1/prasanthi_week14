package com.week14.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SaiKiranGlWeek14Application {

	public static void main(String[] args) {
		SpringApplication.run(SaiKiranGlWeek14Application.class, args);
	}

}
