package com.week14.LibraryRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import com.week14.LibraryEntity.Books;
import java.util.List;

@Repository
public interface Repo extends JpaRepository<Books, Integer>
{
	List<Books> findByBookName(String bookName);
    List<Books> findByAuthor(String author);
}



